(function(){function r(e,n,t){function o(i,f){if(!n[i]){if(!e[i]){var c="function"==typeof require&&require;if(!f&&c)return c(i,!0);if(u)return u(i,!0);var a=new Error("Cannot find module '"+i+"'");throw a.code="MODULE_NOT_FOUND",a}var p=n[i]={exports:{}};e[i][0].call(p.exports,function(r){var n=e[i][1][r];return o(n||r)},p,p.exports,r,e,n,t)}return n[i].exports}for(var u="function"==typeof require&&require,i=0;i<t.length;i++)o(t[i]);return o}return r})()({"/srv/http/templates/kemendikbud-ui/src/src/scripts/components/parallax.component.js":[function(require,module,exports){
'use strict';

var parallax = function parallax() {
    $(window).scroll(function () {
        var wScroll = $(this).scrollTop();

        if (wScroll >= 100 && wScroll < 4796) {
            $('.top__nav').addClass('scrolled');
        } else if (wScroll < 300) {
            $('.scroll-text').css({ 'color': 'white' });
            $('.top__nav').removeClass('scrolled');
        }
    });
};
function myFunction(x) {
    if (x.matches) {
        parallax();
    }
}

var x = window.matchMedia("(min-width: 1024px)");
myFunction(x); // Call listener function at run time
x.addListener(myFunction); // Attach listener function on state changes

},{}],"/srv/http/templates/kemendikbud-ui/src/src/scripts/main.js":[function(require,module,exports){
'use strict';

var _parallax = require('./components/parallax.component');

var _parallax2 = _interopRequireDefault(_parallax);

function _interopRequireDefault(obj) {
    return obj && obj.__esModule ? obj : { default: obj };
}

$('.nav__toggle').click(function () {
    $('.mobile__navigation').toggleClass('active');
    $(this).toggleClass('clicked');
}); // import Muc from 'views/muc'
// import View from 'views/view'
// import Component from 'components/tagihan.component'
// import Mitra from 'components/mitra.component'

},{"./components/parallax.component":"/srv/http/templates/kemendikbud-ui/src/src/scripts/components/parallax.component.js"}]},{},["/srv/http/templates/kemendikbud-ui/src/src/scripts/main.js"])
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm5vZGVfbW9kdWxlcy9icm93c2VyLXBhY2svX3ByZWx1ZGUuanMiLCJzcmMvc2NyaXB0cy9jb21wb25lbnRzL3BhcmFsbGF4LmNvbXBvbmVudC5qcyIsInNyYy9zY3JpcHRzL21haW4uanMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7OztBQ0FBLElBQUksV0FBVyxTQUFYLFFBQVcsR0FBVTtBQUNyQixNQUFBLE1BQUEsRUFBQSxNQUFBLENBQWlCLFlBQVU7QUFDdkIsWUFBSSxVQUFVLEVBQUEsSUFBQSxFQUFkLFNBQWMsRUFBZDs7QUFFQSxZQUFHLFdBQUEsR0FBQSxJQUFrQixVQUFyQixJQUFBLEVBQW9DO0FBQ2hDLGNBQUEsV0FBQSxFQUFBLFFBQUEsQ0FBQSxVQUFBO0FBREosU0FBQSxNQUVNLElBQUksVUFBSixHQUFBLEVBQWtCO0FBQ3BCLGNBQUEsY0FBQSxFQUFBLEdBQUEsQ0FBc0IsRUFBQyxTQUF2QixPQUFzQixFQUF0QjtBQUNBLGNBQUEsV0FBQSxFQUFBLFdBQUEsQ0FBQSxVQUFBO0FBQ0g7QUFSTCxLQUFBO0FBREosQ0FBQTtBQWFBLFNBQUEsVUFBQSxDQUFBLENBQUEsRUFBdUI7QUFDbkIsUUFBSSxFQUFKLE9BQUEsRUFBZTtBQUNYO0FBQ0g7QUFDRjs7QUFFRCxJQUFJLElBQUksT0FBQSxVQUFBLENBQVIscUJBQVEsQ0FBUjtBQUNBLFdBQUEsQ0FBQSxFLENBQWM7QUFDZCxFQUFBLFdBQUEsQ0FBQSxVQUFBLEUsQ0FBMEI7Ozs7O0FDakI1QixJQUFBLFlBQUEsUUFBQSwrQkFBQSxDQUFBOzs7Ozs7OztBQUVBLEVBQUEsY0FBQSxFQUFBLEtBQUEsQ0FBd0IsWUFBVTtBQUM5QixNQUFBLHFCQUFBLEVBQUEsV0FBQSxDQUFBLFFBQUE7QUFDQSxNQUFBLElBQUEsRUFBQSxXQUFBLENBQUEsU0FBQTtBQUZKLENBQUEsRSxDQU5BO0FBQ0E7QUFDQTtBQUNBIiwiZmlsZSI6ImdlbmVyYXRlZC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzQ29udGVudCI6WyIoZnVuY3Rpb24oKXtmdW5jdGlvbiByKGUsbix0KXtmdW5jdGlvbiBvKGksZil7aWYoIW5baV0pe2lmKCFlW2ldKXt2YXIgYz1cImZ1bmN0aW9uXCI9PXR5cGVvZiByZXF1aXJlJiZyZXF1aXJlO2lmKCFmJiZjKXJldHVybiBjKGksITApO2lmKHUpcmV0dXJuIHUoaSwhMCk7dmFyIGE9bmV3IEVycm9yKFwiQ2Fubm90IGZpbmQgbW9kdWxlICdcIitpK1wiJ1wiKTt0aHJvdyBhLmNvZGU9XCJNT0RVTEVfTk9UX0ZPVU5EXCIsYX12YXIgcD1uW2ldPXtleHBvcnRzOnt9fTtlW2ldWzBdLmNhbGwocC5leHBvcnRzLGZ1bmN0aW9uKHIpe3ZhciBuPWVbaV1bMV1bcl07cmV0dXJuIG8obnx8cil9LHAscC5leHBvcnRzLHIsZSxuLHQpfXJldHVybiBuW2ldLmV4cG9ydHN9Zm9yKHZhciB1PVwiZnVuY3Rpb25cIj09dHlwZW9mIHJlcXVpcmUmJnJlcXVpcmUsaT0wO2k8dC5sZW5ndGg7aSsrKW8odFtpXSk7cmV0dXJuIG99cmV0dXJuIHJ9KSgpIiwidmFyIHBhcmFsbGF4ID0gZnVuY3Rpb24oKXtcbiAgICAkKHdpbmRvdykuc2Nyb2xsKGZ1bmN0aW9uKCl7XG4gICAgICAgIHZhciB3U2Nyb2xsID0gJCh0aGlzKS5zY3JvbGxUb3AoKTtcbiAgICAgICAgXG4gICAgICAgIGlmKHdTY3JvbGwgPj0gMTAwICYmIHdTY3JvbGwgPCA0Nzk2KXtcbiAgICAgICAgICAgICQoJy50b3BfX25hdicpLmFkZENsYXNzKCdzY3JvbGxlZCcpXG4gICAgICAgIH1lbHNlIGlmKCB3U2Nyb2xsIDwgMzAwKXtcbiAgICAgICAgICAgICQoJy5zY3JvbGwtdGV4dCcpLmNzcyh7J2NvbG9yJzogJ3doaXRlJ30pXG4gICAgICAgICAgICAkKCcudG9wX19uYXYnKS5yZW1vdmVDbGFzcygnc2Nyb2xsZWQnKVxuICAgICAgICB9XG5cbiAgICB9KVxufVxuZnVuY3Rpb24gbXlGdW5jdGlvbih4KSB7XG4gICAgaWYgKHgubWF0Y2hlcykge1xuICAgICAgICBwYXJhbGxheCgpXG4gICAgfVxuICB9XG4gIFxuICB2YXIgeCA9IHdpbmRvdy5tYXRjaE1lZGlhKFwiKG1pbi13aWR0aDogMTAyNHB4KVwiKVxuICBteUZ1bmN0aW9uKHgpIC8vIENhbGwgbGlzdGVuZXIgZnVuY3Rpb24gYXQgcnVuIHRpbWVcbiAgeC5hZGRMaXN0ZW5lcihteUZ1bmN0aW9uKSAvLyBBdHRhY2ggbGlzdGVuZXIgZnVuY3Rpb24gb24gc3RhdGUgY2hhbmdlcyIsIi8vIGltcG9ydCBNdWMgZnJvbSAndmlld3MvbXVjJ1xuLy8gaW1wb3J0IFZpZXcgZnJvbSAndmlld3Mvdmlldydcbi8vIGltcG9ydCBDb21wb25lbnQgZnJvbSAnY29tcG9uZW50cy90YWdpaGFuLmNvbXBvbmVudCdcbi8vIGltcG9ydCBNaXRyYSBmcm9tICdjb21wb25lbnRzL21pdHJhLmNvbXBvbmVudCdcbmltcG9ydCBQb3B1cCBmcm9tICdjb21wb25lbnRzL3BhcmFsbGF4LmNvbXBvbmVudCdcblxuJCgnLm5hdl9fdG9nZ2xlJykuY2xpY2soZnVuY3Rpb24oKXtcbiAgICAkKCcubW9iaWxlX19uYXZpZ2F0aW9uJykudG9nZ2xlQ2xhc3MoJ2FjdGl2ZScpO1xuICAgICQodGhpcykudG9nZ2xlQ2xhc3MoJ2NsaWNrZWQnKTtcbn0pIl19
