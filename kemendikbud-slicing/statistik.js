const dataFilmDalamTahun = [
  {
    name: "Produksi/Rilis Film dalam pertahun",
    colorByPoint: true,
    data: [
      {
        name: "2014",
        y: 115
      },
      {
        name: "2015",
        y: 122
      },
      {
        name: "2016",
        y: 137
      },
      {
        name: "2017",
        y: 118
      },
      {
        name: "2018",
        y: 145
      },
      {
        name: "2019",
        y: 127
      },
      {
        name: "2020",
        y: 18
      }
    ]
  }
],
    dataIzinFilmDalamTahun = [
      {
        name: "Izin Produksi/Rilis Film dalam pertahun",
        colorByPoint: true,
        data: [
          {
            name: "2014",
            y: 110,
            totalFilm: 115
          },
          {
            name: "2015",
            y: 121,
            totalFilm: 122
          },
          {
            name: "2016",
            y: 130,
            totalFilm: 137
          },
          {
            name: "2017",
            y: 100,
            totalFilm: 118
          },
          {
            name: "2018",
            y: 140,
            totalFilm: 145
          },
          {
            name: "2019",
            y: 127,
            totalFilm: 127
          },
          {
            name: "2020",
            y: 18,
            totalFilm: 18
          }
        ]
      }
    ];

Highcharts.chart('filmDalamTahun', {
  chart: {
    type: 'bar'
  },
  title: {
    text: 'Hasil Penyaringan Data/Informasi'
  },
  subtitle: {
    text: ''
  },
  accessibility: {
    announceNewData: {
      enabled: true
    }
  },
  credits: false,
  xAxis: {
    categories: [
      'Dilarang Menyanyi di Kamar Mandi',
      'Tarung Sarung',
      'Roh Mati Paksa: Cinta Berujung Maut',
      'Djoerig Salawe',
      'Tersanjung the Movie'
    ],
    title: {
      text: null
    }
  },
  yAxis: {
    min: 0,
    title: {
      text: 'Penonton',
      align: 'high'
    },
    labels: {
      overflow: 'justify'
    }
  },
  legend: {
    enabled: false
  },
  plotOptions: {
    series: {
      borderWidth: 0,
      dataLabels: {
        enabled: true,
        format: '{point.y}'
      }
    }
  },

  tooltip: {
    valueSuffix: ' penonton'
  },

  series: [{
    name: 'Penonton',
    colorByPoint: true,
    data: [2500000, 500000, 1500000, 900000, 2200000]
  }]
});

Highcharts.chart('izinFilmDalamTahun', {
  chart: {
    type: 'column'
  },
  title: {
    text: 'Izin Produksi/Rilis Film dalam pertahun'
  },
  subtitle: {
    text: ''
  },
  accessibility: {
    announceNewData: {
      enabled: true
    }
  },
  credits: false,
  xAxis: {
    type: 'category'
  },
  yAxis: {
    title: {
      text: 'Total Izin Produksi/Rilis Film dalam tahun'
    }

  },
  legend: {
    enabled: false
  },
  plotOptions: {
    series: {
      borderWidth: 0,
      dataLabels: {
        enabled: true,
        format: '{point.y}'
      }
    }
  },

  tooltip: {
    headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
    pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b> dari <b>{point.totalFilm}</b> yang rilis<br/>'
  },

  series: dataIzinFilmDalamTahun
});