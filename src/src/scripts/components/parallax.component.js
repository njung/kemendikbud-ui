var parallax = function(){
    $(window).scroll(function(){
        var wScroll = $(this).scrollTop();
        
        if(wScroll >= 100 && wScroll < 4796){
            $('.top__nav').addClass('scrolled')
        }else if( wScroll < 300){
            $('.scroll-text').css({'color': 'white'})
            $('.top__nav').removeClass('scrolled')
        }

    })
}
function myFunction(x) {
    if (x.matches) {
        parallax()
    }
  }
  
  var x = window.matchMedia("(min-width: 1024px)")
  myFunction(x) // Call listener function at run time
  x.addListener(myFunction) // Attach listener function on state changes